package cn.edu.thu.iss;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;

public class LoginController implements Initializable {
	@FXML TextField username;
	@FXML TextField passwd;
	@FXML Label tips;
	@FXML Button login;
	Properties properties=new Properties();
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		File file=new File("iss.config");
		if(file.exists()){
			try {
				properties.load(new FileInputStream(file));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		username.setText(properties.getProperty("user"));
		passwd.setText(properties.getProperty("pass"));
	}
	@FXML public void login() throws Exception{
		login.setText("登录中...");


		new Thread(new Runnable() {
			@Override
			public void run() {
				int result;
				try {
					result = App.getInstance().iss.login2iss(username.getText(),passwd.getText());
					if(result==-1){
						Platform.runLater(new Runnable() {
							@Override
							public void run() {
								tips.setText("用户名或者密码错误，请慎重重试（最多五次）");
								login.setText("登录");
							}
						});
						return;
					}else if(result==-1){
						Platform.runLater(new Runnable() {
							@Override
							public void run() {
								tips.setText("用户名或者密码错误次数过多。。。请15min后再试。。。");
								login.setText("登录");
							}
						});
						return;
					}
					else if(result==-2){
						Platform.runLater(new Runnable() {
							@Override
							public void run() {
								tips.setText("Ops，验证码！请使用浏览器进行登录，并点击保持登录，然后打开软件重试~");
								login.setText("请先在浏览器登录");
							}
						});
						return;
					}
					Platform.runLater(new Runnable() {
						@Override
						public void run() {
							tips.setText("登录成功");
						}
					});

					File file=new File("iss.config");
					properties.setProperty("user", username.getText());
					properties.setProperty("pass", passwd.getText());
					properties.store(new FileOutputStream(file), "iss.config");
					Platform.runLater(new Runnable() {
						@Override
						public void run() {
							try {
								Parent next = FXMLLoader.load(getClass().getResource("main.fxml"));
								Stage stage = App.getInstance().stage;
								Scene scene = new Scene(next);
								stage.setScene(scene);
								stage.show();
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					});
				} catch (Exception e1) {
					e1.printStackTrace();
					Platform.runLater(new Runnable() {
						@Override
						public void run() {
							tips.setText("可能网络有故障，请重启软件并检查网络");
						}
					});
				}

			}
		}).start();;


	}

}
