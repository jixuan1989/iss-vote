package cn.edu.thu.iss;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;

public class UserController implements Initializable {
	@FXML CheckBox name;
	@FXML Label value;
	int id=46;
	public void init(String name,int value,int id){
		this.id=Integer.valueOf(id);
		this.value.setText(value+"");
		this.name.setText(name);
		String[]names=new String[]{"黄向东","罗娜","李小晶","李逸婧","王博磊","Kate Smith","潘红颖"}; 
		for(String n:names){
			if(name.equals(n)){
				this.name.setSelected(true);
				this.name.setStyle("-fx-text-fill: red;");
			}
		}
	}
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
	}
	public void select() {
		name.setSelected(true);
	}
}
