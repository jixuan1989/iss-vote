package cn.edu.thu.iss;

/**
 * create by tingwen.pan
 * 博客地址：http://www.cangzhang.com
 * 说明：本程序是模拟登陆bbs论坛
 */



import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.client.params.CookiePolicy;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.AbstractHttpClient;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class TestISS {
	HttpClient httpclient;
	int progress=0;
	public TestISS(){
		httpclient = new DefaultHttpClient();
		httpclient.getParams().setParameter(ClientPNames.COOKIE_POLICY,  
				CookiePolicy.BROWSER_COMPATIBILITY);  
		//		  	httpclient.getParams().setParameter(CoreProtocolPNames.USER_AGENT,"Mozilla/5.0 (Linux; U; Android 2.2; en-us; Nexus One Build/FRF91) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1");
		httpclient.getParams().setParameter(CoreProtocolPNames.USER_AGENT,"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:18.0) Gecko/20100101 Firefox/18.0");  
		httpclient.getParams().setParameter(    CoreProtocolPNames.HTTP_CONTENT_CHARSET, "UTF-8");  
	}
	public void setHeader(HttpPost httppost){
		httppost.addHeader("User-Agent","Mozilla/5.0 (Windows NT 6.1; WOW64; rv:18.0) Gecko/20100101 Firefox/18.0");  
		httppost.addHeader("Accept","text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");  
		//		httppost.addHeader("Accept-Encoding", "gzip, deflate");  
		httppost.addHeader("Accept-Language","zh-cn,zh;q=0.8,en-us;q=0.5,en;q=0.3");  
		httppost.addHeader("Cache-Control", "max-age=0");  
		httppost.addHeader("Connection", "keep-alive");    
//		httppost.addHeader("Content-Type","text/html;charset=GBK");
		//httppost.getParams().setParameter(HttpMethodParams.HTTP_CONTENT_CHARSET, "UTF-8");
	}
	public void setHeader(HttpGet httpget){
		httpget.addHeader("User-Agent","Mozilla/5.0 (Windows NT 6.1; WOW64; rv:18.0) Gecko/20100101 Firefox/18.0");  
		httpget.addHeader("Accept","text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");  
		//		httpget.addHeader("Accept-Encoding", "gzip, deflate");  
		httpget.addHeader("Accept-Language","zh-cn,zh;q=0.8,en-us;q=0.5,en;q=0.3");  
		httpget.addHeader("Cache-Control", "max-age=0");  
		httpget.addHeader("Connection", "keep-alive"); 
//		httpget.addHeader("Content-Type","text/html;charset=UTF-8");
	}
	// 获取一个HTML页面的内容，一个简单的get应用
	public void grabPageHTML() throws Exception {
		HttpClient httpclient = new DefaultHttpClient();
		HttpGet httpget = new HttpGet("http://www.baidu.com/");
		HttpResponse response = httpclient.execute(httpget);
		HttpEntity entity = response.getEntity();
		String html = EntityUtils.toString(entity, "UTF-8");

		// releaseConnection等同于reset，作用是重置request状态位，为下次使用做好准备。
		// 其实就是用一个HttpGet获取多个页面的情况下有效果；否则可以忽略此方法。
		httpget.releaseConnection();

		System.out.println(html);
	}

	// 下载一个文件到本地（本示范中为一个验证码图片）
	public void downloadFile() throws Exception {
		String url = "http://www.lashou.com/account/captcha";
		File dir = new File("D:\\TDDOWNLOAD");
		if (!dir.exists()) {
			dir.mkdirs();
		}
		String destfilename = "D:\\TDDOWNLOAD\\yz.png";
		HttpClient httpclient = new DefaultHttpClient();
		HttpGet httpget = new HttpGet(url);
		File file = new File(destfilename);
		if (file.exists()) {
			file.delete();
		}

		HttpResponse response = httpclient.execute(httpget);
		HttpEntity entity = response.getEntity();
		InputStream in = entity.getContent();
		try {
			FileOutputStream fout = new FileOutputStream(file);
			int l = -1;
			byte[] tmp = new byte[2048];
			while ((l = in.read(tmp)) != -1) {
				fout.write(tmp);
			}
			fout.close();
		} finally {
			// 在用InputStream处理HttpEntity时，切记要关闭低层流。
			in.close();
		}
		httpget.releaseConnection();
	}

	
	public static void main(String[]args) throws Exception{
		TestISS test=new TestISS();
		test.login2iss("","");
//		System.out.println(test.vote("46"));
//		Map<String, Pair> users=test.listAllUsers();
//		System.out.println(users);
	}
	public Map<String,Pair> listAllUsers() throws ClientProtocolException, IOException{
		Map<String, Pair> rawMap=new HashMap<String, Pair>();
			
		Map<String, Pair> usersMap=new TreeMap<String, Pair>(new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				return rawMap.get(o1).value>rawMap.get(o2).value?-1:1;
			}
		});
		for(int i=1;i<=8;i++){
			this.progress=i;
			String url="http://cssclub.cn/takashiro_vote-main.html?orderby=selfintro&page="+i;
			System.out.println(url);
			HttpGet httpget = new HttpGet(url);
			setHeader(httpget);
			HttpResponse response = httpclient.execute(httpget);
			HttpEntity entity = response.getEntity();
			String postResult = EntityUtils.toString(entity, "UTF-8");
			Document document=Jsoup.parse(postResult);
			Element element=document.getElementById("ct");
			Elements els=element.getElementsByTag("li");
			for (Element e:els){
				Elements idE=e.getElementsByClass("tile");
				Elements nameE=e.getElementsByClass("realname");
				Elements valueE=e.getElementsByClass("value");
				String name="",value=""; String id="";
				for(Element tmp0: idE){
					id=tmp0.attr("data-cid");
				}
				for(Element tmp1: nameE){
					name=tmp1.text();
				}
				for(Element tmp2: valueE){
					value=tmp2.html();
				}
				if(!name.isEmpty()&&!value.isEmpty()){
					Pair pair=new Pair(Integer.valueOf(id),Integer.valueOf(value));
					rawMap.put(name, pair);
					usersMap.put(name, pair);
				}
			}
			httpget.releaseConnection();
		}
		return usersMap;
	}


	// Post方法，模拟表单提交参数登录到网站。
	// 结合了上面两个方法：grabPageHTML/downloadFile，同时增加了Post的代码。
	public int  login2iss(String username, String passwd) throws Exception {
		//        // 第一步：先下载验证码到本地
		//        String url = "http://www.lashou.com/account/captcha";
		//        String destfilename = "./yz.png";

		HttpResponse response;
		HttpEntity entity ;

		HttpPost httppost = new HttpPost("http://cssclub.cn/member.php?mod=logging&action=login&loginsubmit=yes&frommessage&loginhash=LVW58");
		setHeader(httppost);
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("username", username));
		params.add(new BasicNameValuePair("password", passwd));
		params.add(new BasicNameValuePair("cookietime", "true"));
		httppost.setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));

		response = httpclient.execute(httppost);
		entity = response.getEntity();
		// 在这里可以用Jsoup之类的工具对返回结果进行分析，以判断登录是否成功
		String postResult = EntityUtils.toString(entity, "UTF-8");
		System.out.println(postResult);
		if(postResult.contains("登录失败，您还可以尝试")){
			System.out.println("用户名或者密码错误");
			httppost.releaseConnection();
			return -1;
		}else if(postResult.contains("密码错误次数过多")){
			System.out.println("用户名或者密码错误次数过多。。。");
			httppost.releaseConnection();
			return -3;
		}else if(postResult.contains("密码空或包含非法字符")){
			System.out.println("密码空或包含非法字符");
			httppost.releaseConnection();
			return -1;
		}

		// 我们这里只是简单的打印出当前Cookie值以判断登录是否成功。
		CookieStore cookieStore = ((AbstractHttpClient) httpclient).getCookieStore();
		List<Cookie> cookies = ((AbstractHttpClient) httpclient)
				.getCookieStore().getCookies();
//		for (Cookie cookie : cookies)
//			System.out.println("cookie begin***\n" + cookie + "\n cookie end");
		System.out.println("post result:");
		if(postResult.contains("验证码")){
			return -2;
		}
		httppost.releaseConnection();
		return 0;
	}

	public boolean vote(String number) throws ClientProtocolException, IOException{
		HttpGet httpget = new HttpGet("http://cssclub.cn/plugin.php?id=takashiro_vote:main&action=vote&votedfield=selfintro&cid="+number);
		setHeader(httpget);
		HttpResponse response = httpclient.execute(httpget);
		HttpEntity entity = response.getEntity();
		String postResult = EntityUtils.toString(entity, "UTF-8");
		if (postResult.equals("1")){
			return true;
		}else{
			return false;
		}

	}


	// 下载一个文件到本地（本示范中为一个验证码图片）
	public void downloadFile(String url) throws Exception {
		File dir = new File("./ecode");
		if (!dir.exists()) {
			dir.mkdirs();
		}
		String destfilename = "./ecode\\1.png";
		HttpGet httpget = new HttpGet(url);

		File file = new File(destfilename);
		if (file.exists()) {
			file.delete();
		}
		setHeader(httpget);
		HttpResponse response = httpclient.execute(httpget);
		HttpEntity entity = response.getEntity();
		InputStream in = entity.getContent();
		try {
			FileOutputStream fout = new FileOutputStream(file);
			int l = -1;
			byte[] tmp = new byte[2048];
			while ((l = in.read(tmp)) != -1) {
				fout.write(tmp);
			}
			fout.close();
		} finally {
			// 在用InputStream处理HttpEntity时，切记要关闭低层流。
			in.close();
		}
		httpget.releaseConnection();
	}
	class Pair{
		int id;
		int value;
		public Pair(int id, int value){
			this.id=id;
			this.value=value;
		}
		public String toString(){
			return "(id,v)=("+id+","+value+")";
		}
	}
}
