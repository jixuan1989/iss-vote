package cn.edu.thu.iss;


import java.io.IOException;

import org.apache.http.client.HttpClient;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

@SuppressWarnings("restriction")
public class App extends Application {
	TestISS iss;
	Stage stage;
	static App instance;
	@Override
	public void start(Stage stage) throws IOException {
		this.stage=stage;
		instance=this;
		iss=new TestISS();
		Parent root = FXMLLoader.load(getClass().getResource("login.fxml"));
		
        Scene scene = new Scene(root, 640, 401);
    
        stage.setTitle("ISS 十佳自动投票工具v0.1 （作者黄向东）");
        stage.setScene(scene);
        stage.show();
        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			@Override
			public void handle(WindowEvent event) {
				// TODO Auto-generated method stub
				Platform.exit();
				System.exit(1);
			}
		});
        
	}
	public static void main(String[] args) {
		launch(args);
	}
	public static App getInstance() {
		return instance;
	}
}
