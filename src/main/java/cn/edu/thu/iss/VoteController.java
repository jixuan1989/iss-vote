package cn.edu.thu.iss;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;

import org.apache.http.client.ClientProtocolException;

import cn.edu.thu.iss.TestISS.Pair;

public class VoteController implements Initializable{
	@FXML GridPane grib;
	@FXML Label tips;
	@FXML Button flush;
	@FXML Button autorun;
	@FXML TextField scheduletime;
	TreeMap<String, TestISS.Pair> users  =null;
	UserController[][] userControllers=new UserController[6][10];
	Properties properties=new Properties();
	List<Pane> userPanes=new ArrayList<Pane>();
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		grib.setGridLinesVisible(true);
		File file=new File("iss.config");
		if(file.exists()){
			try {
				properties.load(new FileInputStream(file));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	FileWriter writer=null;
	File logs=new File("vote-history.txt");
	public void autorun(){
		Calendar calendar=Calendar.getInstance();
		tips.setText("自动投票时间,每日："+calendar.get(Calendar.HOUR_OF_DAY)+"时");
		if(autorun.getText().equals("立即投票")){
			task();
		}else{
			ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();
			service.scheduleWithFixedDelay(new Runnable() {
				@Override
				public void run() {
					task();
				}
			}, 1, 3600*24, TimeUnit.SECONDS);
		}
	}

	public void task(){

		autorun.setDisable(true);
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					writer=new FileWriter(logs, true);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				File file=new File("iss.config");
				if(file.exists()){
					try {
						properties.load(new FileInputStream(file));
						String ids=(String) properties.get("vote");
						if(ids==null){
							save();
							properties.load(new FileInputStream(file));
							ids=(String) properties.get("vote");
						}
						if(!ids.isEmpty()){
							String[] id=ids.split(",");
							for(String i:id){
								try{
									App.getInstance().iss.vote(i);
									if(writer!=null){
										writer.write(new Date()+"为id为"+i+"的选手投了一票成功\n");
										System.out.println(new Date()+"为id为"+i+"的选手投了一票成功");
									}
								}catch(Exception ee){
									ee.printStackTrace();
									tips.setText("可能网络故障了。。投票失败，请重启软件");
								}
							}
						}else{
							tips.setText("您尚未保存勾选信息吧？");
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				else{
					tips.setText("不要删除iss.config。。。。。");
				}

				Platform.runLater(new Runnable() {
					@Override
					public void run() {
						autorun.setDisable(false);
						autorun.setText("立即投票");
					}
				});
				try {
					writer.close();
				} catch (IOException e) {
					e.printStackTrace();
				}

				new Thread(new Runnable() {
					@Override
					public void run() {
						Platform.runLater(new Runnable() {
							@Override
							public void run() {
								try {
									flush();
								} catch (IOException e) {
									e.printStackTrace();
								}
							}
						});
					}
				}).start();

			}
		}).start();

	}
	public void flush() throws ClientProtocolException, IOException{
		if(!flush.isDisabled()){
			Set<String> selectedIds=new HashSet<String>();
			if(properties!=null&&properties.get("vote")!=null){
				for(String id:properties.get("vote").toString().split(",")){
					selectedIds.add(id);
				}
			}
			flush.setText("约30s");
			flush.setDisable(true);
			grib.getChildren().removeAll(userPanes);
			userPanes.clear();
			new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						//					new Thread(new Runnable() {
						//						@Override
						//						public void run() {
						//							try {
						//								Thread.sleep(2000);
						//							} catch (InterruptedException e) {
						//								e.printStackTrace();
						//							}
						//							Platform.runLater(new Runnable() {
						//								@Override
						//								public void run() {
						//									flush.setText(App.getInstance().iss.progress+"/8");
						//								}
						//							});
						//						}
						//					}).start();
						users  = (TreeMap<String, Pair>) App.getInstance().iss.listAllUsers();
						//System.out.println("[debug]:"+users);
						Platform.runLater(new Runnable() {
							@Override
							public void run() {
								Iterator<String> userIterator=users.keySet().iterator();
								Iterator<TestISS.Pair> valueIterator=users.values().iterator();
								for(int col=0;col<6;col++){
									for(int row=0;row<10;row++){
										FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("user.fxml"));
										Pane userRoot;
										if(userIterator.hasNext()){
											String user=userIterator.next();
											TestISS.Pair pair=valueIterator.next();
											try {
												userRoot = (Pane)fxmlLoader.load();
												userPanes.add(userRoot);
												GridPane.setConstraints(userRoot, col, row);
												GridPane.setMargin(userRoot, new Insets(10, 10, 10, 10));
												GridPane.setHalignment(userRoot, HPos.CENTER);
												userControllers[col][row]=(UserController)fxmlLoader.getController();
												userControllers[col][row].init(user, pair.value, pair.id);
												if(selectedIds.contains(pair.id+"")){
													userControllers[col][row].select();
												}
												grib.getChildren().add(userRoot);
											} catch (IOException e) {
												e.printStackTrace();
											}
										}
									}
								}
								flush.setText("刷新列表");
								flush.setDisable(false);
							}
						});
					} catch (IOException e1) {
						e1.printStackTrace();
						Platform.runLater(new Runnable() {
							@Override
							public void run() {
								tips.setText("获取选手列表错误。。建议重启软件尝试。。。");
							}
						});
					}

				}
			}).start();;
		}

	}
	public void save() throws FileNotFoundException, IOException{
		String ids="";
		for(UserController[] users:userControllers){
			for(UserController user:users){
				if(user!=null){
					if(user.name.isSelected()){
						ids+=user.id+",";
					}
				}
			}
		}
		if(!ids.isEmpty()){
			properties.setProperty("vote",ids.substring(0,ids.length()-1));

		}
		properties.store(new FileOutputStream("iss.config"), "iss.config");
	}
}
